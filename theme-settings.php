<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function yg_dentalcare_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['yg_dentalcare_settings'] = [
    '#type' => 'details',
    '#title' => t('YG Dental Care Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'bootstrap',
    '#weight' => 10,
  ];
  // Social links.
  $form['yg_dentalcare_settings']['contact_info'] = [
    '#type' => 'details',
    '#title' => t('Contact Info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_dentalcare_settings']['contact_info']['contact_title'] = [
    '#type' => 'textfield',
    '#title' => t('Contact Info Title'),
    '#description' => t('Please enter contact info title'),
    '#default_value' => theme_get_setting('contact_title'),
    '#required' => TRUE,
  ];
  $form['yg_dentalcare_settings']['contact_info']['address1'] = [
    '#type' => 'textfield',
    '#title' => t('Address 1'),
    '#description' => t('Please enter your address url'),
    '#default_value' => theme_get_setting('address1'),
  ];
  $form['yg_dentalcare_settings']['contact_info']['address2'] = [
    '#type' => 'textfield',
    '#title' => t('Address 2'),
    '#description' => t('Please enter your address2 url'),
    '#default_value' => theme_get_setting('address2'),
  ];
  $form['yg_dentalcare_settings']['contact_info']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['yg_dentalcare_settings']['contact_info']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['yg_dentalcare_settings']['contact_info']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your instagram url'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];
  // Footer.
  $form['yg_dentalcare_settings']['footer'] = [
    '#type' => 'details',
    '#title' => t('Footer'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_dentalcare_settings']['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];
  $footer_description = theme_get_setting('footer_description');
  $form['yg_dentalcare_settings']['footer']['footer_description'] = [
    '#type' => 'text_format',
    '#title' => t('Footer Description'),
    '#description' => t('Please enter footer description...'),
    '#default_value' => $footer_description['value'],
    '#foramt'        => $footer_description['format'],
  ];

}
